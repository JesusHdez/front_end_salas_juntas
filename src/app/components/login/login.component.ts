import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  registerForm: FormGroup;
  errMsg: any
  isRegister: boolean = false
  registerErrors = {
    nombre: false,
    apellidos: false,
    contrasena: false,
    confirmar_contrasena: false,
    email: false,
    password_equals_validations: false
  }

  constructor(private user: UserService, private router: Router, public snackBar: MatSnackBar,) {
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });
    this.registerForm = new FormGroup({
      nombre: new FormControl(),
      apellidos: new FormControl(), 
      email: new FormControl(),
      password: new FormControl(),
      confirmPassword: new FormControl()
    });
    this.errMsg = ''
  }
  

  ngOnInit(){
    if(localStorage.getItem('user_type_salaJuntas') === '1' || localStorage.getItem('user_type_salaJuntas') === '2'){
      localStorage.getItem('user_type_salaJuntas') === '1' ? this.router.navigate([`/admin`]) : this.router.navigate([`/user`])  
    }
  }

  login(){
    this.errMsg = ''
    if(this.loginForm.controls['email'].value !== null && this.loginForm.controls['password'].value !== null){
      this.user.login(this.loginForm.controls['email'].value, this.loginForm.controls['password'].value ).subscribe(
        (data) => {
          console.log(data)
          localStorage.setItem('id_user_salaJuntas', data._id);
          localStorage.setItem('user_type_salaJuntas', data.tipo_usuario)
  
          if(data.tipo_usuario === 1){
            this.router.navigate([`/admin`])
          }else{
            this.router.navigate([`/user`])
          }
        }, (err) => {
          this.errMsg = err.error.msg
        }
      )
    }else{
      this.errMsg = 'Ingresa tu usuario y contrasena'
    }

  }

  register(){
    this.registerForm.controls['nombre'].value === "" ? this.registerErrors.nombre = true : this.registerErrors.nombre = false
    this.registerForm.controls['apellidos'].value === "" ? this.registerErrors.apellidos = true : this.registerErrors.apellidos = false
    this.registerForm.controls['password'].value === "" ? this.registerErrors.contrasena = true : this.registerErrors.contrasena = false
    this.registerForm.controls['confirmPassword'].value === "" ? this.registerErrors.confirmar_contrasena = true : this.registerErrors.confirmar_contrasena = false
    this.registerForm.controls['confirmPassword'].value === this.registerForm.controls['password'].value ? this.registerErrors.password_equals_validations = false : this.registerErrors.password_equals_validations = true

    if(this.registerForm.controls['email'].value === ""){
      this.registerErrors.email = true
    }else{
      let EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if (this.registerForm.controls['email'].value.match(EMAIL_REGEX)){
        this.registerErrors.email = false
      }else{
        this.registerErrors.email = true
      }
    }

    
    if(!this.registerErrors.nombre && !this.registerErrors.apellidos && !this.registerErrors.contrasena && !this.registerErrors.confirmar_contrasena){
      if(!this.registerErrors.email){
        this.showSnackBar("Ingresa un email valido")
      }else if(this.registerErrors.password_equals_validations){
        this.showSnackBar("Las contraseñas deben ser iguales")
      }else{
        let body = {
          nombre: this.registerForm.controls['nombre'].value,
          apellidos: this.registerForm.controls['apellidos'].value, 
          email: this.registerForm.controls['email'].value,
          password: this.registerForm.controls['password'].value
        }
        this.user.postUser(body).subscribe(
          (data) => {
            this.isRegister = false
            this.showSnackBar('Registro exitoso!')
          }, (error) => {
            this.showSnackBar(error.error.msg !== undefined && error.error.msg !== null ? error.error.msg : 'Ocurrio algo inesperado, intentalo nuevamente!')
          }
        )
      }
    }else{
      this.showSnackBar("Rellena todos los campos para continuar")
    }
    
    
  }

  showSnackBar(msg: any){
    this.snackBar.open(msg, 'OK!', { duration: 5000, verticalPosition: 'bottom' });
  }

  openForm(){
    this.registerForm.controls['nombre'].setValue('')
    this.registerForm.controls['apellidos'].setValue('') 
    this.registerForm.controls['email'].setValue('')
    this.registerForm.controls['password'].setValue('')
    this.registerForm.controls['confirmPassword'].setValue('')

    this.isRegister = true
  }

}
