import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { StatesService } from 'src/app/services/states.service';

@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.css']
})
export class StatesComponent implements OnInit {
  createSala: boolean = false
  editSala: boolean = false
  editSalaId: any
  sala: any
  displayedColumns: string[] = ['nombre', 'pais', 'acciones'];
  salaForm: FormGroup

  constructor(private statesService: StatesService) {
    this.sala = []
    this.salaForm = new FormGroup({
      nombre: new FormControl(),
      pais: new FormControl(),
    });
  }

  ngOnInit(){
    this.getSalas()
  }

  getSalas(){
    this.statesService.getSalas().subscribe(
      (data) => {
        this.sala = data
        console.log(this.sala)
      }
    )
  }

  deleteSala(id: any){
    this.statesService.deleteSala(id).subscribe(
      (data) => {
        this.getSalas()
      }      
    )
  }

  putSala(sala: any){
    this.showCreate()
    this.editSala = true
    this.editSalaId = sala._id
    this.salaForm.controls['nombre'].setValue(sala.nombre)
    this.salaForm.controls['pais'].setValue(sala.pais)
  }

  newSala(){
    let body = {
      nombre: this.salaForm.controls['nombre'].value,
      pais: 'Mexico' 
    }
    if(this.editSala){
      this.editSala = false
      this.statesService.putSala(body, this.editSalaId).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }else{
      this.statesService.createSala(body).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }
    
  }

  showCreate(){
    this.createSala = !this.createSala
    this.salaForm.reset()
    this.getSalas()
  }
}
