import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { SalaService } from 'src/app/services/sala.service'
@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  createSala: boolean = false
  editSala: boolean = false
  editSalaId: any
  sala: any
  displayedColumns: string[] = ['nombre', 'ubicacion', 'descripcion', 'capacidad_maxima', 'acciones'];
  salaForm: FormGroup

  constructor(private salaService: SalaService) {
    this.sala = []
    this.salaForm = new FormGroup({
      nombre: new FormControl(),
      ubicacion: new FormControl(),
      descripcion: new FormControl(),
      capacidad_maxima: new FormControl()
    });
  }

  ngOnInit(){
    this.getSalas()
  }

  getSalas(){
    this.salaService.getSalas().subscribe(
      (data) => {
        this.sala = data
        console.log(this.sala)
      }
    )
  }

  deleteSala(id: any){
    this.salaService.deleteSala(id).subscribe(
      (data) => {
        this.getSalas()
      }      
    )
  }

  putSala(sala: any){
    this.showCreate()
    this.editSala = true
    this.editSalaId = sala._id
    this.salaForm.controls['nombre'].setValue(sala.nombre)
    this.salaForm.controls['ubicacion'].setValue(sala.ubicacion)
    this.salaForm.controls['descripcion'].setValue(sala.descripcion)
    this.salaForm.controls['capacidad_maxima'].setValue(sala.capacidad_maxima)
  }

  newSala(){
    let body = {
      nombre: this.salaForm.controls['nombre'].value,
      ubicacion: this.salaForm.controls['ubicacion'].value, 
      descripcion: this.salaForm.controls['descripcion'].value,
      capacidad_maxima: this.salaForm.controls['capacidad_maxima'].value,
    }
    if(this.editSala){
      this.editSala = false
      this.salaService.putSala(body, this.editSalaId).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }else{
      this.salaService.createSala(body).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }
    
  }

  showCreate(){
    this.createSala = !this.createSala
    this.salaForm.reset()
    this.getSalas()
  }

}
