import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isAdmin: boolean = false

  constructor(private router: Router) {}

  ngOnInit(): void {
    if(localStorage.getItem('user_type_salaJuntas') === '1'){
      this.isAdmin = true
    }else{
      this.isAdmin = false
    }
  }

  openMyMeets(){
    this.router.navigate([`/user/my_meets`])
  }

  openSchedules(){
    this.router.navigate([`/admin/schedules`])
  }

  openStates(){
    this.router.navigate([`/admin/states`])
  }

  openCities(){
    this.router.navigate([`/admin/cities`])
  }

  openRooms(opc: any){
    if(opc === 1){
      this.router.navigate([`/admin`])
    }else{
      this.router.navigate([`/user`])
    }
  }

  logout(){
    localStorage.setItem('id_user_salaJuntas', '');
    localStorage.setItem('user_type_salaJuntas', '')
    this.router.navigate([`/login`])
  }

}
