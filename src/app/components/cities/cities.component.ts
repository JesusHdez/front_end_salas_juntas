import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, Validators } from '@angular/forms';
import { CitiesService } from 'src/app/services/cities.service';
import { StatesService } from 'src/app/services/states.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  createSala: boolean = false
  editSala: boolean = false
  editSalaId: any
  sala: any
  displayedColumns: string[] = ['nombre', 'estado', 'pais', 'acciones'];
  salaForm: FormGroup
  stateControl: any
  estados: any
  selectedState: String = ''

  constructor(private citiesService: CitiesService, private estadosService: StatesService) {
    this.sala = []
    this.estados = []
    this.salaForm = new FormGroup({
      nombre: new FormControl(),
      estado: new FormControl(),
    });
    this.stateControl = new FormControl('', Validators.required);
  }

  ngOnInit(){
    this.getSalas()
  }

  getSalas(){
    this.citiesService.getSalas().subscribe(
      (data) => {
        this.sala = data
        console.log(this.sala)
      }
    )
  }

  deleteSala(id: any){
    this.citiesService.deleteSala(id).subscribe(
      (data) => {
        this.getSalas()
      }      
    )
  }

  putSala(sala: any){
    this.showCreate()
    this.editSala = true
    this.editSalaId = sala._id
    this.salaForm.controls['nombre'].setValue(sala.nombre)
    this.salaForm.controls['estado'].setValue(sala.estado)
  }

  newSala(){
    console.log(this.stateControl)
    let body = {
      nombre: this.salaForm.controls['nombre'].value,
      estado: this.stateControl.value.nombre,
      estado_id: this.stateControl.value._id,
      pais: 'Mexico' 
    }
    if(this.editSala){
      this.editSala = false
      this.citiesService.putSala(body, this.editSalaId).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }else{
      this.citiesService.createSala(body).subscribe(
        (data) => {
          this.salaForm.reset()
          this.showCreate()
        }
      )
    }
    
  }

  getEstados(){
    this.estadosService.getSalas().subscribe(
      (data) => {
        console.log(data)
        this.estados = data
      }
    )
  }

  showCreate(){
    if(!this.createSala){
      this.getEstados()
    }else{
      this.getSalas()
    }
    this.createSala = !this.createSala
    this.salaForm.reset()

  }
}

interface State{
  nombre: String, 
  _id: String
}
