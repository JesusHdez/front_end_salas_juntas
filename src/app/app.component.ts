import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Co Working';

  constructor(private router: Router){}

  showNavbar(){
    if (this.router.url.indexOf('admin') === -1 && this.router.url.indexOf('user') === -1){
        return true;
    } else {
      return false;
    }
  }
}
