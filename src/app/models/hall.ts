export class Hall{
    _id?: number;
    name: string;
    description: string;
    max_person: number;

    constructor(name: string, description: string, max_person: number){
        this.name = name
        this.description = description
        this.max_person = max_person
    }
}