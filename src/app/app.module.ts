import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { AdminViewComponent } from './components/admin-view/admin-view.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { NavbarComponent } from './components/navbar/navbar/navbar.component';
import { MyMeetsComponent } from './components/my-meets/my-meets.component';
import { SchedulesComponent } from './components/schedules/schedules.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule}  from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { StatesComponent } from './components/states/states.component';
import { CitiesComponent } from './components/cities/cities.component' 
import {MatSelectModule} from '@angular/material/select'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminViewComponent,
    UserViewComponent,
    NavbarComponent,
    MyMeetsComponent,
    SchedulesComponent,
    StatesComponent,
    CitiesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    ReactiveFormsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule, 
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    FormsModule,
    MatSelectModule,
    NoopAnimationsModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
