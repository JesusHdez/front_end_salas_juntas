import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {

  constructor(private router: Router) {}

  async canActivate() {
    if(localStorage.getItem('user_type_salaJuntas') === '1'){
      return true
    }else{
      this.router.navigate([`/`]) 
      return false
    }
  }
  
}
