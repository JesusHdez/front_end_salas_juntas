import { Injectable } from '@angular/core';
import { Router, CanActivate, } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthUserGuard implements CanActivate {
  constructor(private router: Router) {}

  async canActivate() {
    if(localStorage.getItem('user_type_salaJuntas') === '2'){
      return true
    }else{
      this.router.navigate([`/`]) 
      return false
    }
  }
}
