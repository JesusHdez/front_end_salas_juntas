import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { base_url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SalaService {

  constructor(private http: HttpClient) {}
  getSalas(): Observable<any>{
    return this.http.get(base_url+'sala');
  }

  deleteSala(id: any): Observable<any>{
    return this.http.delete(base_url+'sala/'+id);
  }

  createSala(body: any): Observable<any>{
    return this.http.post(base_url+'sala', body);
  }

  putSala(body: any, id: any): Observable<any>{
    return this.http.put(base_url+'sala/'+id, body);
  }
}
