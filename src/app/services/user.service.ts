import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { base_url } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient){}

    login(email: String, password: String): Observable<any>{
        return this.http.post(base_url+'login', {email: email, password: password});
      }
      
    postUser(body: any): Observable<any>{
        return this.http.post(base_url+'user', {body});
    }

    getUser(): Observable<any>{
        return this.http.get(base_url+'user/64e84550bce058d29fa24c5c')
    }
}