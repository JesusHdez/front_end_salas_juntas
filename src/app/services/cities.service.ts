import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { base_url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  constructor(private http: HttpClient) {}
  getSalas(): Observable<any>{
    return this.http.get(base_url+'ciudad');
  }

  deleteSala(id: any): Observable<any>{
    return this.http.delete(base_url+'ciudad/'+id);
  }

  createSala(body: any): Observable<any>{
    return this.http.post(base_url+'ciudad', body);
  }

  putSala(body: any, id: any): Observable<any>{
    return this.http.put(base_url+'ciudad/'+id, body);
  }
}
