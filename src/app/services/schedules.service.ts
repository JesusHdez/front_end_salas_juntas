import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { base_url } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) {}

  getAllSchedules(): Observable<any>{
    return this.http.get(base_url+'schedules');
  }
  
  getActiveSchedules(): Observable<any>{
    return this.http.get(base_url+'active_schedules');
  }

  deleteSchedule(id: any): Observable<any>{
    return this.http.delete(base_url+'schedules/'+id);
  }

  createSchedule(body: any): Observable<any>{
    return this.http.post(base_url+'schedule', body);
  }

  putSchedule(body: any, id: any): Observable<any>{
    return this.http.put(base_url+'schedules/'+id, body);
  }
}
