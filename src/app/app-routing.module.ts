import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AdminViewComponent } from './components/admin-view/admin-view.component';
import { AuthAdminGuard } from './guards/auth-admin.guard';
import { AuthUserGuard } from './guards/auth-user.guard';
import { UserViewComponent } from './components/user-view/user-view.component';
import { SchedulesComponent } from './components/schedules/schedules.component';
import { MyMeetsComponent } from './components/my-meets/my-meets.component';
import { StatesComponent } from './components/states/states.component';
import { CitiesComponent } from './components/cities/cities.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'admin', component: AdminViewComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/schedules', component: SchedulesComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/states', component: StatesComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/cities', component: CitiesComponent, canActivate: [AuthAdminGuard] },
  { path: 'user', component: UserViewComponent, canActivate: [AuthUserGuard] },
  { path: 'user/my_meets', component: MyMeetsComponent, canActivate: [AuthUserGuard] },
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
